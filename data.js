let tech_data = {
	"articles" : [
	    {
	    	"id" : "1",
	    	"nom" : "Casque audio",
	    	"marque" : "Casio",
	    	"tailles_modele" : ["XS","S","M","L"],
	    	"prix" : ["18","18","18","22"],
	    	"info" : "Vehicula Ridiculus Mattis Tristique Pharetra",
	    	"detail" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Donec sed odio dui. Nullam id dolor id nibh ultricies vehicula ut id elit.",
	    	"livraison" : "2 à 3 jours après confirmation de la commande",
	    	"image" : "audio-casque.png"
	    },
	    {
	    	"id" : "2",
	    	"nom" : "Microphone sans fil",
	    	"marque" : "LG",
	    	"tailles_modele" : ["Unique"],
	    	"prix" : ["99"],
	    	"info" : "Inceptos Parturient Sollicitudin Lorem Ridiculus",
	    	"detail" : "Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Cras justo odio, dapibus ac facilisis in, egestas eget quam.",
	    	"livraison" : "2 à 3 jours après confirmation de la commande",
	    	"image" : "audio-micro.png"
	    },
	    {
	    	"id" : "3",
	    	"nom" : "Calculatrice",
	    	"marque" : "Texas Instruments",
	    	"tailles_modele" : ["Collège", "Lycée"],
	    	"prix" : ["45.50","52.50"],
	    	"info" : "Ultricies Inceptos Amet Ornare",
	    	"detail" : "Nulla vitae elit libero, a pharetra augue. Etiam porta sem malesuada magna mollis euismod. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Donec id elit non mi porta gravida at eget metus.",
	    	"livraison" : "2 à 3 jours après confirmation de la commande",
	    	"image" : "calculatrice.png"
	    },
	    {
	    	"id" : "4",
	    	"nom" : "Ecran",
	    	"marque" : "Iyama",
	    	"tailles_modele" : ["15 pouces","17 pouces","19 pouces","21 pouces"],
	    	"prix" : ["100","120","200","300"],
	    	"info" : "Dapibus Cursus Ullamcorper Inceptos Purus",
	    	"detail" : "Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Sed posuere consectetur est at lobortis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean lacinia bibendum nulla sed consectetur.",
	    	"livraison" : "2 à 3 jours après confirmation de la commande",
	    	"image" : "ecran.png"
	    },
	    {
	    	"id" : "5",
	    	"nom" : "Manette de jeu",
	    	"marque" : "Microsoft",
	    	"tailles_modele" : ["Sans-fil"],
	    	"prix" : ["80"],
	    	"info" : "Malesuada Fusce Ipsum Elit Parturient",
	    	"detail" : "Aenean lacinia bibendum nulla sed consectetur. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Donec sed odio dui. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Maecenas sed diam eget risus varius blandit sit amet non magna. Maecenas sed diam eget risus varius blandit sit amet non magna. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.",
	    	"livraison" : "2 à 3 jours après confirmation de la commande",
	    	"image" : "manette-jeu.png"
	    },
	    {
	    	"id" : "6",
	    	"nom" : "Oridnateur de bureau",
	    	"marque" : "Dell",
	    	"tailles_modele" : ["Ordi complet"],
	    	"prix" : ["780"],
	    	"info" : "Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.",
	    	"detail" : "Sed posuere consectetur est at lobortis. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Maecenas sed diam eget risus varius blandit sit amet non magna.",
	    	"livraison" : "2 à 3 jours après confirmation de la commande",
	    	"image" : "ordinateur-bureau.png"
	    },
	    {
	    	"id" : "7",
	    	"nom" : "Oridnateur portable",
	    	"marque" : "HP",
	    	"tailles_modele" : ["15 pouces"],
	    	"prix" : ["650"],
	    	"info" : "Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.",
	    	"detail" : "Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras mattis consectetur purus sit amet fermentum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.",
	    	"livraison" : "2 à 3 jours après confirmation de la commande",
	    	"image" : "ordinateur-portable.png"
	    },
	    {
	    	"id" : "8",
	    	"nom" : "Tour d'ordinateur",
	    	"marque" : "Apple",
	    	"tailles_modele" : ["Serveur"],
	    	"prix" : ["1300"],
	    	"info" : "Risus Commodo Ridiculus Purus Fermentum",
	    	"detail" : "Aenean lacinia bibendum nulla sed consectetur. Vestibulum id ligula porta felis euismod semper. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Maecenas sed diam eget risus varius blandit sit amet non magna. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Nullam id dolor id nibh ultricies vehicula ut id elit. Vestibulum id ligula porta felis euismod semper.",
	    	"livraison" : "2 à 3 jours après confirmation de la commande",
	    	"image" : "ordinateur-tour.png"
	    },
	    {
	    	"id" : "9",
	    	"nom" : "Robot",
	    	"marque" : "Wishtime",
	    	"tailles_modele" : ["v2"],
	    	"prix" : ["60"],
	    	"info" : "Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.",
	    	"detail" : "Curabitur blandit tempus porttitor. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.",
	    	"livraison" : "2 à 3 jours après confirmation de la commande",
	    	"image" : "robot.png"
	    }
	]
}

var categories_data = {
    "boutique" : [
        { "id" : "ordi", "nom" : "Ordinateur/Électronique", "stock" : "9"},
        { "id" : "sport", "nom" : "Sport/Jeux", "stock" : "10"},
        { "id" : "cuisine", "nom" : "Ustensiles de cuisine", "stock" : "0"},
        { "id" : "habillement", "nom" : "Habillement", "stock" : "0"},
        { "id" : "velo", "nom" : "Accessoires de vélo", "stock" : "0"}
    ]
}


var sport_data = {
	"articles" : [
	    {
	    	"id" : "1",
	    	"nom" : "Casque de football Américain",
	    	"marque" : "Adidas",
	    	"tailles_modele" : ["56","58","60"],
	    	"prix" : ["25","25","27"],
	    	"info" : "Purus Bibendum Tristique Dapibus Consectetur",
	    	"detail" : "Etiam porta sem malesuada magna mollis euismod. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.",
	    	"livraison" : "1 semaine après confirmation de la commande",
	    	"image" : "american-football-casque.png"
	    },
	    {
	    	"id" : "2",
	    	"nom" : "Sac de jeu de société",
	    	"marque" : "Adidas",
	    	"tailles_modele" : ["Unique"],
	    	"prix" : ["30"],
	    	"info" : "Inceptos Parturient Nullam Etiam Magna",
	    	"detail" : "Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.",
	    	"livraison" : "1 à 3 jours après confirmation de la commande",
	    	"image" : "baggs-jeu.png"
	    },
	    {
	    	"id" : "3",
	    	"nom" : "Balle de football",
	    	"marque" : "Nike",
	    	"tailles_modele" : ["5","7"],
	    	"prix" : ["20","25"],
	    	"info" : "Tortor Sollicitudin Pharetra Ullamcorper Sit",
	    	"detail" : "Donec ullamcorper nulla non metus auctor fringilla. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.",
	    	"livraison" : "1 à 3 jours après confirmation de la commande",
	    	"image" : "balle-football.png"
	    },
	    {
	    	"id" : "4",
	    	"nom" : "Balles de golf",
	    	"marque" : "Golf Genius",
	    	"tailles_modele" : ["6 balles","12 balles","24 balles"],
	    	"prix" : ["16","30","55"],
	    	"info" : "Vestibulum id ligula porta felis euismod semper.",
	    	"detail" : "Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Maecenas sed diam eget risus varius blandit sit amet non magna. Donec ullamcorper nulla non metus auctor fringilla. Maecenas faucibus mollis interdum.",
	    	"livraison" : "1 à 3 jours après confirmation de la commande",
	    	"image" : "balle-golf.png"
	    },
	    {
	    	"id" : "5",
	    	"nom" : "Balles de tennis",
	    	"marque" : "Dunlop",
	    	"tailles_modele" : ["4 balles","8 balles"],
	    	"prix" : ["6.90","12"],
	    	"info" : "Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Aenean lacinia bibendum nulla sed consectetur.",
	    	"detail" : "Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Sed posuere consectetur est at lobortis. Maecenas faucibus mollis interdum. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.",
	    	"livraison" : "1 à 3 jours après confirmation de la commande",
	    	"image" : "balle-sport-tennis.png"
	    },
	    {
	    	"id" : "6",
	    	"nom" : "Raquette de tennis de table",
	    	"marque" : "Dunlop",
	    	"tailles_modele" : ["La paire"],
	    	"prix" : ["13.50"],
	    	"info" : "Etiam Risus Adipiscing Purus Quam",
	    	"detail" : "Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Maecenas faucibus mollis interdum.",
	    	"livraison" : "1 à 3 jours après confirmation de la commande",
	    	"image" : "raquette-tennis-table.png"
	    },
	    {
	    	"id" : "7",
	    	"nom" : "Jeu Backgammon",
	    	"marque" : "Middleton Games",
	    	"tailles_modele" : ["Bois rare"],
	    	"prix" : ["105"],
	    	"info" : "Porta Quam Vulputate Euismod Nibh",
	    	"detail" : "Aenean lacinia bibendum nulla sed consectetur. Donec ullamcorper nulla non metus auctor fringilla. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.",
	    	"livraison" : "1 à 3 jours après confirmation de la commande",
	    	"image" : "jeu-backgammon.png"
	    },
	    {
	    	"id" : "8",
	    	"nom" : "Gant de boxe",
	    	"marque" : "Adidas",
	    	"tailles_modele" : ["XS","S","M","L","XL"],
	    	"prix" : ["22.5","25","30","35","40"],
	    	"info" : "Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.",
	    	"detail" : "Curabitur blandit tempus porttitor. Maecenas faucibus mollis interdum. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.",
	    	"livraison" : "1 semaine après confirmation de la commande",
	    	"image" : "la-boxe-gants.png"
	    },
	    {
	    	"id" : "9",
	    	"nom" : "Casque de baseball",
	    	"marque" : "Nike",
	    	"tailles_modele" : ["56","58","60"],
	    	"prix" : ["20","20","25"],
	    	"info" : "Fermentum Dapibus Quam Magna Vehicula",
	    	"detail" : "Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum id ligula porta felis euismod semper. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Cras mattis consectetur purus sit amet fermentum. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.",
	    	"livraison" : "1 à 3 jours après confirmation de la commande",
	    	"image" : "le-baseball-casque.png"
	    },
	    {
	    	"id" : "10",
	    	"nom" : "Manette de jeu",
	    	"marque" : "Microsoft",
	    	"tailles_modele" : ["Sans-fil"],
	    	"prix" : ["80"],
	    	"info" : "Malesuada Fusce Ipsum Elit Parturient",
	    	"detail" : "Aenean lacinia bibendum nulla sed consectetur. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Donec sed odio dui. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Maecenas sed diam eget risus varius blandit sit amet non magna. Maecenas sed diam eget risus varius blandit sit amet non magna. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.",
	    	"livraison" : "2 à 3 jours après confirmation de la commande",
	    	"image" : "manette-jeu.png"
	    }
	]
}
