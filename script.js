/**************************
 * File: script.js
 * Author: Gael Marcadet
 * Version: 1.0
 * Description: Dynamic page 
 **************************/
function loadJSON( json_filename, callback ) {
	console.log( "LOADING" )
	var xobj = new XMLHttpRequest();
	xobj.overrideMimeType("application/json");
	xobj.open('GET', json_filename, true);
	xobj.onreadystatechange = function () {
		if (xobj.readyState == 4 && xobj.status == "200") {
			// Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
			callback(xobj.responseText);
		}
	};
	xobj.send(null);
}

function resize_container( container, size ) {
	for ( let i = 1; i <= 12; ++i ) {
		container.removeClass( 'col-' + i );
	}
	container.addClass( 'col-' + size );
}

/**
 * Returns a new category wrapped in jquery object.
 * 
 * @param {*} id Category identifier.
 * @param {*} name Category name.
 * @param {*} stock Category stock.
 */
function create_category( id, name, stock ) {
	let category = $( '#prototypes .category-prototype' ).clone();
	category.data( 'target', id );
	category.find( 'img' ).attr( 'src', './resources/images/' + id + '.png' );
	category.find( '.name' ).text( name );
	category.find( '.stock' ).text( stock );

	return category;
}	

/**
 * Displays categories inside specified container.
 * 
 * @param {*} container HTMLElement
 * @param {*} categories JSON list
 */
function display_categories( container, categories ) {
	for ( let index = 0; index < categories.length; index++ ) {
		let category = categories[ index ];
		container.append( create_category( category.id, category.nom, category.stock ) );
	}
}

function create_item( item, resources_folder ) {
	var item_content = '' +
	'<li class="item list-group-item" data-item-id="' + item.id + '">' +
		'<div class="row">' +
			'<div class="col-1">' +
				'<img src="' + resources_folder  + '/' + item.image + '" alt=""/>' +
			'</div>' +  
			'<div class="col-11 description">' + item.nom + '</div>' +
		'</div>' +
	'</li>'; 
	return $( item_content )
}

function display_items( container, items_container, resources_folder ) {
	items = items_container.articles;
	for ( let index = 0; index < items.length; index++ ) {
		let item = items[ index ];
		container.append( create_item( item, resources_folder ) )
	}
}


function display_items_from_category( container, category ) {
	// initialze container to receive items
	container.empty();
	// diplays items from catego
	let target = category.data( "target" );
	
	if ( target === "ordi" ) {
		display_items( container, tech_data, './resources/ordi' );
	} else if ( target === "sport" ) {
		display_items( container, sport_data, './resources/sport' );
	} else {
		let no_items = $( '<div class="list-group"> <div class="list-group-item">Il n\'y a aucun article dans cette catégorie </div> </div>' );
		container.append( no_items );	
	}
	
}

function display_item( container, category, item, item_data ) {
	// update header
	container.find('.item-image').attr( 'src', item.find( 'img' ).attr( 'src' ) );
	container.find( '.item-name' ).text( item_data.nom );
	container.find( '.item-info' ).text( item_data.info );

	// update item properties
	container.find( '.item-brand' ).text( item_data.marque );
	container.find( '.item-delivery' ).text( item_data.livraison );

	// updates sizes
	let size_rows = container.find( '.size-rows' );
	let size_selection = container.find( '.size-selection' );
	let input_quantity = container.find( '.item-quantity' );
	size_rows.empty();
	size_selection.empty();
	for ( let index = 0; index < item_data.tailles_modele.length; index++ ) {
		let size = item_data.tailles_modele[ index ];
		let price = item_data.prix[ index ];

		// display size
		let new_row = $('<tr> <td>' + size + '</td><td>' + price + '</td> </tr>');
		size_rows.append( new_row );

		// add size in selection
		let option_size = $( '<option>' + size + '</option>' );
		option_size.data( 'price', price );
		size_selection.append( option_size );
		
		// initialize quantity input
		input_quantity.val( 1 );
	}
}

$(document).ready(function () {
	let categories = $( '#categories' );
	let items = $( '#items' );
	let item_detail = $( '#item-detail' );

	// loads and displays categories
	display_categories( categories, categories_data.boutique )

	// handles category click
	let displayed_category = null;
	categories.find( ".category" ).click( function() {
		// split screen to contain categories and items
		resize_container( categories, 6 );
		resize_container( items, 6 );
		item_detail.addClass( 'hidden' );

		
		// displays items from targeted category
		display_items_from_category( items, $(this) );
		displayed_category = $(this);
	} )

	// handles item click
	items.click( '.item', function( event ) {
		item = $( event.target ).closest( '.item' );
		if ( displayed_category ) {
			// resize containers to display item detail
			resize_container( categories, 4 );
			resize_container( items, 4 );
			item_detail.removeClass( 'hidden' );
			
			// found item data
			let category_name = displayed_category.data( 'target' );
			let searched_item_owner = category_name == 'sport' ? sport_data : tech_data;
			for ( let index = 0; index < searched_item_owner.articles.length; index++  ) {
				if ( searched_item_owner.articles[ index ].id == item.data( 'item-id' ) ) {
					display_item( item_detail, displayed_category, item, searched_item_owner.articles[ index ] );
				}
			}
		}
	} );

	// handles item quantity buttons click event
	function update_quantity( input, operation ) {
		// updates value depending on passed operation
		try {
			let current_value = parseInt( input.val() );
			let updated_value = operation( current_value );
			if ( 1 <= updated_value ) {
				input.val( updated_value )
			}
		} catch (error) {
			input.val( 1 );
		}
	}

	let input_quantity = $( '.item-quantity' );
	// enable button modification quantity
	$( 'button.add-item' ).click( function() {
		update_quantity( input_quantity, function( n ) { return n + 1 } );
	} );

	$( 'button.remove-item' ).click( function() {
		update_quantity( input_quantity, function( n ) { return n - 1 } );
	} );

	// handles item ordering
	$( '#order-button' ).click( function( ) {
		let ordered_quantity = parseInt( $( '.item-quantity' ).val() );
		let price = parseInt($( 'option:selected' ).data( 'price' ));
		alert( ordered_quantity * price );
	} )
});